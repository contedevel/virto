#!/bin/bash
host="http://localhost:8080"

function create_user() {
	local response=$(curl -s -X POST -H "Content-Type: application/json" \
	-d '{"first_name":"'$1'", "last_name":"'$2'", "email":"'$3'", 
	"birthdate":"'$4'", "sex":"'$5'", "role":"'$6'", "password":"123@Qwerty",
	"password_confirmation":"123@Qwerty"}' $host/users/new)
	python3 -c "import json; print(json.loads('$response')['id'])"
}

function create_user_with_experiences() {
	local response=$(curl -s -X POST -H "Content-Type: application/json" \
	-d '{"first_name":"'$1'", "last_name":"'$2'", "email":"'$3'", 
	"birthdate":"'$4'", "sex":"'$5'", "role":"'$6'", "password":"123@Qwerty",
	"password_confirmation":"123@Qwerty", "experiences":['$7']}' \
	$host/users/new)
	python3 -c "import json; print(json.loads('$response')['id'])"
}

function create_game() {
	local response=$(curl -s -X POST -H "Content-Type: application/json" \
	-d '{"name":"'$1'", "description":"'$2'", "user_id":"'$3'"}' \
	$host/games/new)
	python3 -c "import json; print(json.loads('$response')['id'])"
}

function create_achievement() {
	local response=$(curl -s -X POST -H "Content-Type: application/json" \
	-d '{"name":"'$3'", "description":"'$4'"}' \
	$host/games/$1/achievements/new?user_id=$2)
	python3 -c "import json; print(json.loads('$response')['id'])"
}

function create_exprience() {
	local response=$(curl -s -X POST -H "Content-Type: application/json" \
	-d '{"user_id":"'$1'", "game_id":"'$2'", "achievement_id":"'$3'", 
	"value":"'$4'"}' $host/experiences/new)
	python3 -c "import json; print(json.loads('$response')['id'])"
}
###############################################################################
# Users
###############################################################################
denis_id=$(create_user "Denis" "Sologub" "contedevel2010@gmail.com" \
	"30.08.1993" "Male" "Admin")
artem_id=$(create_user "Artem" "Sologub" "artemsologub24@gmail.com" \
	"24.06.1998" "Male" "Admin")
jie_id=$(create_user "Jie" "Sologub" "18845767392@gmail.com" "04.11.1995" \
	"Female" "User")
arma_id=$(create_user "Arma" "Sologub" "arma93@gmail.com" "19.09.1993" \
	"Male" "User")
tanya_id=$(create_user "Tanya" "Sologub" "t.a.n.y.a@gmail.com" "10.03.2000" \
	"Female" "User")
vera_id=$(create_user "Vera" "Sologub" "verchik@gmail.com" "31.08.1991" \
	"Female" "User")
###############################################################################
# Games
###############################################################################
game_id=$(create_game 'Minecraft' 'Game' $denis_id)
###############################################################################
# Achievements
###############################################################################
ach_id1=$(create_achievement $game_id $denis_id "Achievement1" "Test")
ach_id2=$(create_achievement $game_id $denis_id "Achievement2" "Test")
###############################################################################
# Experiences
###############################################################################
exp_id1=$(create_exprience $denis_id $game_id $ach_id1 "88600")
exp_id2=$(create_exprience $artem_id $game_id $ach_id1 "600")
exp_id3=$(create_exprience $jie_id $game_id $ach_id1 "886")
exp_id4=$(create_exprience $arma_id $game_id $ach_id1 "8")
exp_id5=$(create_exprience $vera_id $game_id $ach_id1 "0")
exp_id6=$(create_exprience $tanya_id $game_id $ach_id1 "1000")
###############################################################################
# Users with experiences
###############################################################################
json='{"game_id":"'$game_id'","achievement_id":"'$ach_id1'","value":"900"}'
bob_id=$(create_user_with_experiences "Bob" "Jackson" "bob@gmail.com" \
	"23.01.1985" "Male" "User" $json)
