package com.contedevel.virto.server.utils;

import com.contedevel.virto.server.config.ServerConfig;
import com.contedevel.virto.server.models.UserKeys;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public class Validator {

    private Request mRequest = new Request();

    public boolean validateUser(UUID userId) {
        String url = String.format("%s/users/%s", ServerConfig.USERS_HOST, userId);
        return validateResponse(url);
    }

    public boolean validateAdmin(UUID userId) {
        String url = String.format("%s/users/%s", ServerConfig.USERS_HOST, userId);
        ResponseEntity<String> entity = mRequest.get(url);

        if (entity.getStatusCode() == HttpStatus.OK) {
            String body = entity.getBody();
            JSONObject json = new JSONObject(body);
            String role = json.getString(UserKeys.ROLE);

            return "Admin".equals(role);
        }

        return false;
    }

    public boolean validateGame(UUID gameId) {
        String url = String.format("%s/games/%s", ServerConfig.GAMES_HOST, gameId);
        return validateResponse(url);
    }

    public boolean validateAchievement(UUID gameId, UUID achievementId) {
        String url = String.format("%s/games/%s/achievements/%s",
                ServerConfig.GAMES_HOST, gameId, achievementId);
        return validateResponse(url);
    }

    private boolean validateResponse(String url) {
        ResponseEntity<String> entity = mRequest.get(url);

        return entity != null && entity.getStatusCode() == HttpStatus.OK;
    }
}
