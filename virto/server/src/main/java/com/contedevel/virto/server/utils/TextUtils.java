package com.contedevel.virto.server.utils;

public final class TextUtils {

    private TextUtils() { }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }

    public static boolean isNullOrWhitespace(String text) {
        return text == null || text.trim().isEmpty();
    }
}
