package com.contedevel.virto.server.utils;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class Request {

    private RestTemplate mRestTemplate = new RestTemplate();

    public ResponseEntity<String> post(String url, Object request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> entity = new HttpEntity<>(request, headers);

        return mRestTemplate.postForEntity(url, entity, String.class);
    }

    public ResponseEntity<String> get(String url) {
        return mRestTemplate.getForEntity(url, String.class);
    }

    public ResponseEntity<String> delete(String url) {
        return mRestTemplate.exchange(url, HttpMethod.DELETE,
                HttpEntity.EMPTY, String.class);
    }
}
