package com.contedevel.virto.server.models;

public abstract class AchievementKeys extends Keys {

    public static final String TABLE_NAME = "achievements";

    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String GAME_ID = "game_id";
}
