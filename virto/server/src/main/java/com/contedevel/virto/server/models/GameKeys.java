package com.contedevel.virto.server.models;

public abstract class GameKeys extends Keys {

    public static final String TABLE_NAME = "games";

    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String USER_ID = "user_id";
}
