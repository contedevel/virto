package com.contedevel.virto.server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@JsonRootName("error")
public class ApiError {

    @JsonProperty("status")
    private HttpStatus mStatus;
    @JsonProperty("message")
    private String mMessage;
    @JsonProperty("errors")
    private List<String> mErrors;

    public ApiError(HttpStatus status, String message, List<String> errors) {
        mStatus = status;
        mMessage = message;
        mErrors = errors;
    }

    public ApiError(HttpStatus status, String message, String error) {
        this(status, message, Arrays.asList(error));
    }

    public HttpStatus getStatus() {
        return mStatus;
    }

    public String getMessage() {
        return mMessage;
    }

    public List<String> getErrors() {
        return mErrors;
    }
}
