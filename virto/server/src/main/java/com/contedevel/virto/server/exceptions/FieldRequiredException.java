package com.contedevel.virto.server.exceptions;

public class FieldRequiredException extends IllegalArgumentException {

    public FieldRequiredException() {
        super();
    }

    public FieldRequiredException(String s) {
        super(s);
    }

    public FieldRequiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldRequiredException(Throwable cause) {
        super(cause);
    }
}
