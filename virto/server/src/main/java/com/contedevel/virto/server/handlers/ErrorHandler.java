package com.contedevel.virto.server.handlers;

import com.contedevel.virto.server.exceptions.FieldRequiredException;
import com.contedevel.virto.server.exceptions.IllegalFieldException;
import com.contedevel.virto.server.models.ApiError;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = Logger.getLogger(ErrorHandler.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        LOG.error(ex.getMessage(), ex);
        List<String> errors = new ArrayList<>();
        BindingResult result = ex.getBindingResult();

        for (FieldError e : result.getFieldErrors()) {
            errors.add(e.getField() + ": " + e.getDefaultMessage());
        }

        for (ObjectError e : result.getGlobalErrors()) {
            errors.add(e.getObjectName() + ": " + e.getDefaultMessage());
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getLocalizedMessage(), errors);

        return handleExceptionInternal(ex, apiError, headers,
                apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        final String errorPattern = "No handle found for %s %s";
        String error = String.format(errorPattern, ex.getHttpMethod(), ex.getRequestURL());
        LOG.error(ex.getMessage(), ex);
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getMessage(), error);

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex) {
        LOG.error(ex.getMessage(), ex);
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR,
                ex.getMessage(), "error occurred");

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({
            IllegalFieldException.class,
            FieldRequiredException.class
    })
    public ResponseEntity<Object> handleFieldExceptions(Exception ex) {
        LOG.error(ex.getMessage(), ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getMessage(), "");

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        LOG.error(error, ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getMessage(), error);

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex
    ) {
        LOG.error(ex.getMessage(), ex);
        List<String> errors = new ArrayList<>();
        final String errorPattern = "%s %s: %s";

        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            String error = String.format(errorPattern,
                    violation.getRootBeanClass().getName(),
                    violation.getPropertyPath(), violation.getMessage());
            errors.add(error);
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getMessage(), errors);

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex
    ) {
        String error = String.format("%s should be of type %s", ex.getName(),
                ex.getRequiredType().getName());
        LOG.error(error, ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
                ex.getMessage(), error);

        return new ResponseEntity<>(apiError, new HttpHeaders(),
                apiError.getStatus());
    }
}
