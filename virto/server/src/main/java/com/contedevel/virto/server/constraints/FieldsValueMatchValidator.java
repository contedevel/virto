package com.contedevel.virto.server.constraints;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldsValueMatchValidator
        implements ConstraintValidator<FieldsValueMatch, Object> {

    private String mField;
    private String mFieldMatch;

    public void initialize(FieldsValueMatch constraintAnnotation) {
        mField = constraintAnnotation.field();
        mFieldMatch = constraintAnnotation.fieldMatch();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
        BeanWrapperImpl impl = new BeanWrapperImpl(value);
        Object fieldValue = impl.getPropertyValue(mField);
        Object fieldMatchValue = impl.getPropertyValue(mFieldMatch);

        if (fieldValue != null) {
            return fieldValue.equals(fieldMatchValue);
        }

        return fieldMatchValue == null;
    }
}