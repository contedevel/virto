package com.contedevel.virto.server.config;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class ServerConfig {

    private static final String FILENAME = "server.properties";

    private static final Logger LOGGER = Logger.getLogger(ServerConfig.class);

    public static final String EXPERIENCES_HOST;
    public static final String USERS_HOST;
    public static final String GAMES_HOST;

    static {
        Properties properties = new Properties();
        boolean isOk = true;

        try {
            InputStream is = ServerConfig.class
                    .getClassLoader()
                    .getResourceAsStream(FILENAME);
            properties.load(is);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            isOk = false;
            String msg = String.format("File '%s' not found.", FILENAME);
            LOGGER.error(msg, e);
        } catch (IOException e) {
            e.printStackTrace();
            isOk = false;
            LOGGER.error("Server configuration wasn't loaded. " +
                    "Default configuration is used.", e);
        }

        if (isOk) {
            EXPERIENCES_HOST = properties.getProperty("experiences_host");
            USERS_HOST = properties.getProperty("users_host");
            GAMES_HOST = properties.getProperty("games_host");
        } else {
            EXPERIENCES_HOST = "http://localhost:8081";
            USERS_HOST = "http://localhost:8082";
            GAMES_HOST = "http://localhost:8083";
        }
    }

    private ServerConfig() {

    }
}
