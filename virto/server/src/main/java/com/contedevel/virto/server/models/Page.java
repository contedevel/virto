package com.contedevel.virto.server.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonRootName("page")
public class Page<T> {

    @JsonProperty("items")
    private List<T> mItems;
    @JsonProperty("number")
    private long mPageNumber;
    @JsonProperty("count")
    private long mPagesCount;

    public Page(List<T> items, long pageNumber, long pagesCount) {
        mItems = items;
        mPageNumber = pageNumber;
        mPagesCount = pagesCount;
    }
}
