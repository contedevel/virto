package com.contedevel.virto.server.exceptions;

public class IllegalFieldException extends IllegalArgumentException {

    public IllegalFieldException() {
        super();
    }

    public IllegalFieldException(String s) {
        super(s);
    }

    public IllegalFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalFieldException(Throwable cause) {
        super(cause);
    }
}
