package com.contedevel.virto.server.constraints;

import com.contedevel.virto.server.utils.TextUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotNullValidator implements ConstraintValidator<NotNullOrEmpty, Object> {

    @Override
    public void initialize(NotNullOrEmpty constraintAnnotation) {

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        if (value != null) {

            if (value instanceof String) {
                String str = (String) value;
                return !TextUtils.isNullOrWhitespace(str);
            }

            return true;
        }

        return false;
    }
}
