package com.contedevel.virto.server.models;

public abstract class UserKeys extends Keys {

    public static final String TABLE_NAME = "users";

    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String ROLE = "role";
    public static final String SEX = "sex";
    public static final String BIRTHDATE = "birthdate";
    public static final String HASH = "hash";
    public static final String SALT = "salt";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_CONFIRMATION = "password_confirmation";

}
