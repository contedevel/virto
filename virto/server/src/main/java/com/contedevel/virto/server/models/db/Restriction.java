package com.contedevel.virto.server.models.db;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

public interface Restriction<T> {
    Expression<Boolean> restrict(CriteriaBuilder builder, Root<T> root);
}
