package com.contedevel.virto.server.models;

public abstract class ExperienceKeys extends Keys {

    public static final String TABLE_NAME = "experiences";

    public static final String GAME_ID = "game_id";
    public static final String ACHIEVEMENT_ID = "achievement_id";
    public static final String USER_ID = "user_id";
    public static final String VALUE = "value";
    public static final String CREATED_ON = "created_on";
    public static final String MODIFIED_ON = "modified_on";
}
