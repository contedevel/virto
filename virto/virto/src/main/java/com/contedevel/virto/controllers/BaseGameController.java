package com.contedevel.virto.controllers;

import com.contedevel.virto.server.config.ServerConfig;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public abstract class BaseGameController extends BaseController {

    protected ResponseEntity<String> getAchievement(UUID gameId, UUID id) {
        String url = String.format("%s/games/%s/achievements/%s",
                ServerConfig.GAMES_HOST, gameId, id);

        return request.get(url);
    }
}
