package com.contedevel.virto.utils;

import com.contedevel.virto.server.exceptions.IllegalFieldException;
import com.contedevel.virto.server.models.ExperienceKeys;
import com.contedevel.virto.server.models.UserKeys;
import com.contedevel.virto.server.utils.Pair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public final class UserUtils {

    private UserUtils() {}

    public static Pair<String, List<String>> getExperience(String newUserJson) {
        List<String> experiences = new ArrayList<>();

        try {
            JSONObject newUser = new JSONObject(newUserJson);
            final String experiencesKey = "experiences";

            if (newUser.has(experiencesKey)) {
                JSONArray exps = newUser.getJSONArray(experiencesKey);

                for (int i = 0; i < exps.length(); ++i) {
                    experiences.add(exps.get(i).toString());
                }

                newUser.remove(experiencesKey);
                newUserJson = newUser.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Parameter must be a JSON-object");
        }

        return new Pair<>(newUserJson, experiences);
    }

    public static String getUserId(String userJson) {

        try {
            JSONObject user = new JSONObject(userJson);
            return user.getString(UserKeys.ID);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalFieldException("Field 'id' not found.");
        }
    }

    public static String addUserId(String json, String userId) {

        try {
            JSONObject obj = new JSONObject(json);
            obj.put(ExperienceKeys.USER_ID, userId);
            return obj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Parameter must be a JSON-object");
        }
    }

    public static String buildResponse(String userJson,
                                       List<String> experiencesJson) {
        try {
            JSONObject user = new JSONObject(userJson);
            JSONArray exps = new JSONArray();

            for (String expJson : experiencesJson) {
                JSONObject exp = new JSONObject(expJson);
                exp.remove(ExperienceKeys.USER_ID);
                exps.put(exp);
            }

            user.put("experiences", exps);
            return user.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Parameter must be a JSON-object");
        }
    }
}
