package com.contedevel.virto.controllers;

import com.contedevel.virto.server.config.ServerConfig;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("experiences")
@RestController("experiences_controller")
public class ExperienceController extends BaseController {

    private static final String ROOT_URL = ServerConfig.EXPERIENCES_HOST
            + "/experiences";

    @PostMapping(value = "new")
    public ResponseEntity<String> add(@RequestBody String newExperience) {
        String url = ROOT_URL + "/new";
        return request.post(url, newExperience);
    }

    @GetMapping(params = { "game_id", "user_id" })
    public ResponseEntity<String> experiences(
            @RequestParam("game_id") UUID gameId,
            @RequestParam("user_id") UUID userId,
            @RequestParam(defaultValue = "0") int page
    ) {
        String url = String.format("%s?game_id=%s&user_id=%s&page=%d",
                ROOT_URL, gameId, userId, page);
        return request.get(url);
    }

    @GetMapping(params = { "game_id", "achievement_id" })
    public ResponseEntity<String> experiences(
            @RequestParam("game_id") UUID gameId,
            @RequestParam("achievement_id") UUID achievementId,
            @RequestParam(value = "asc", defaultValue = "true") boolean isAsc,
            @RequestParam(defaultValue = "0") int page
    ) {
        String url = String.format("%s?game_id=%s&achievement_id=%s&asc=%b&page=%d",
                ROOT_URL, gameId, achievementId, isAsc, page);
        return request.get(url);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<String> experience(@PathVariable UUID id) {
        String url = ROOT_URL + "/" + String.valueOf(id);
        return request.get(url);
    }

    @GetMapping(value = "experience")
    public ResponseEntity<String> experience(
            @RequestParam("user_id") UUID userId,
            @RequestParam("game_id") UUID gameId,
            @RequestParam("achievement_id") UUID achievementId
    ) {
        String url = String.format("%s?user_id=%s&game_id=%s&achievement_id=%s",
                ROOT_URL, userId, gameId, achievementId);
        return request.get(url);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> remove(@PathVariable UUID id) {
        String url = ROOT_URL + "/" + String.valueOf(id);
        return request.delete(url);
    }
}
