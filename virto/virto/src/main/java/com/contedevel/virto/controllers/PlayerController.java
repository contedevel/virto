package com.contedevel.virto.controllers;

import com.contedevel.virto.server.config.ServerConfig;
import com.contedevel.virto.errors.ServerError;
import com.contedevel.virto.server.exceptions.InternalServerException;
import com.contedevel.virto.utils.GameUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("players")
@RestController("player_controller")
public class PlayerController extends BaseGameController {

    @GetMapping(value = "top", produces = "application/json")
    public ResponseEntity<String> topUsers(
            @RequestParam("game_id") UUID gameId,
            @RequestParam("achievement_id") UUID achievementId,
            @RequestParam int count
    ) {
        ResponseEntity<String> achievement = getAchievement(gameId, achievementId);

        if (achievement.getStatusCode() != HttpStatus.OK) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        String top = GameUtils.buildTopUsers(request, achievement.getBody(),
                gameId, achievementId, count);

        return new ResponseEntity<>(top, HttpStatus.OK);
    }

    @GetMapping(value = "{user_id}/games/{game_id}/achievements/max")
    public ResponseEntity<String> maxAchievement(
            @PathVariable("user_id") UUID userId,
            @PathVariable("game_id") UUID gameId
    ) {
        String url = String.format("%s/experiences/max?user_id=%s&game_id=%s",
                ServerConfig.EXPERIENCES_HOST, userId, gameId);
        ResponseEntity<String> experience = request.get(url);

        if (experience.getStatusCode() != HttpStatus.OK) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        String experienceJson = experience.getBody();
        UUID achievementId = GameUtils.getAchievementId(experienceJson);

        return getAchievement(gameId, achievementId);
    }
}
