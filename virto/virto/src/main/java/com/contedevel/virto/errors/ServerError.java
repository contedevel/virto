package com.contedevel.virto.errors;

public final class ServerError {

    public static final String INTERNAL = "Internal server error.";

    private ServerError() {

    }
}
