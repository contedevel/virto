package com.contedevel.virto.utils;

import com.contedevel.virto.server.config.ServerConfig;
import com.contedevel.virto.errors.ServerError;
import com.contedevel.virto.server.exceptions.InternalServerException;
import com.contedevel.virto.server.models.ExperienceKeys;
import com.contedevel.virto.server.models.UserKeys;
import com.contedevel.virto.server.utils.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public final class GameUtils {

    private GameUtils() {

    }

    public static String buildTopUsers(Request request, String achievementJson, UUID gameId,
                                       UUID achievementId, int count) {

        String expUrlFormat = String.format("%s/experiences" +
                        "?game_id=%s&achievement_id=%s&asc=false",
                ServerConfig.EXPERIENCES_HOST, gameId, achievementId);
        expUrlFormat += "&page=%d";

        int page = 0;

        try {
            JSONObject achievement = new JSONObject(achievementJson);
            JSONArray users = new JSONArray();

            for (int rest = count; rest > 0;) {
                String url = String.format(expUrlFormat, page++);
                ResponseEntity<String> experiences = request.get(url);

                if (experiences.getStatusCode() != HttpStatus.OK) {
                    throw new InternalServerException(ServerError.INTERNAL);
                }

                JSONObject exps = new JSONObject(experiences.getBody());
                JSONArray expValues = exps.getJSONArray("items");
                int size = Math.min(rest, expValues.length());
                rest -= size;
                String userUrlFormat = ServerConfig.USERS_HOST + "/users/";

                for (int i = 0; i < size; ++i) {
                    String userId = expValues.getJSONObject(i)
                            .getString(ExperienceKeys.USER_ID);
                    String userUrl = userUrlFormat + userId;
                    ResponseEntity<String> user = request.get(userUrl);

                    if (user.getStatusCode() != HttpStatus.OK) {
                        throw new InternalServerException(ServerError.INTERNAL);
                    }

                    JSONObject usrJson = new JSONObject(user.getBody());
                    usrJson.remove(UserKeys.ROLE);
                    usrJson.remove(UserKeys.BIRTHDATE);
                    usrJson.remove(UserKeys.SEX);
                    users.put(usrJson);
                }
            }

            achievement.put("players", users);

            return achievement.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static UUID getAchievementId(String json) {

        try {
            JSONObject obj = new JSONObject(json);
            String achievementId = obj.getString(ExperienceKeys.ACHIEVEMENT_ID);
            return UUID.fromString(achievementId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Argument must be a JSON-object");
        }
    }
}
