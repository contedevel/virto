package com.contedevel.virto.controllers;

import com.contedevel.virto.server.config.ServerConfig;
import com.contedevel.virto.errors.ServerError;
import com.contedevel.virto.server.exceptions.InternalServerException;
import com.contedevel.virto.server.utils.Pair;
import com.contedevel.virto.utils.UserUtils;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequestMapping("users")
@RestController
public class UserController extends BaseController {

    @PostMapping(value = "new")
    public ResponseEntity<String> register(@RequestBody String json) {
        Pair<String, List<String>> data = UserUtils.getExperience(json);
        String userUrl = ServerConfig.USERS_HOST + "/users/new";
        String expUrl = ServerConfig.EXPERIENCES_HOST + "/experiences/new";
        ResponseEntity<String> response = request.post(userUrl, data.first);

        if (response.getStatusCode() != HttpStatus.CREATED) {
            return response;
        }

        String userId = UserUtils.getUserId(response.getBody());
        List<String> responses = new ArrayList<>();

        for (String exp : data.second) {
            exp = UserUtils.addUserId(exp, userId);
            ResponseEntity<String> resp = request.post(expUrl, exp);

            if (resp.getStatusCode() != HttpStatus.CREATED) {
                throw new InternalServerException(ServerError.INTERNAL);
            }

            responses.add(resp.getBody());
        }

        if (responses.size() > 0) {
            String body = UserUtils.buildResponse(response.getBody(), responses);
            return new ResponseEntity<>(body, HttpStatus.CREATED);
        }

        return response;
    }

    @GetMapping
    public ResponseEntity<String> all(
            @RequestParam(defaultValue = "0") int page
    ) {
        String url = ServerConfig.USERS_HOST + "/users?page=" + page;
        return request.get(url);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<String> profile(@PathVariable UUID id) {
        String url = ServerConfig.USERS_HOST + "/users/" + String.valueOf(id);
        return request.get(url);
    }
}
