package com.contedevel.virto.controllers;

import com.contedevel.virto.server.config.ServerConfig;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("games")
@RestController
public class GameController extends BaseGameController {

    private static final String ROOT_URL = ServerConfig.GAMES_HOST + "/games";

    @PostMapping(value = "{gameId}/achievements/new")
    public ResponseEntity<String> add(@PathVariable UUID gameId,
                              @RequestParam("user_id") UUID userId,
                              @RequestBody String newAchievement) {
        String url = String.format("%s/%s/achievements/new?user_id=%s",
                ROOT_URL, gameId, userId);
        return request.post(url, newAchievement);
    }

    @PostMapping(value = "new")
    public ResponseEntity<String> add(@RequestBody String newGame) {
        String url = ROOT_URL + "/new";
        return request.post(url, newGame);
    }

    @GetMapping
    public ResponseEntity<String> games(
            @RequestParam(defaultValue = "0") int page
    ) {
        String url = ROOT_URL + "?page=" + page;
        return request.get(url);
    }

    @GetMapping(value = "{gameId}/achievements")
    public ResponseEntity<String> achievements(
            @PathVariable UUID gameId,
            @RequestParam(defaultValue = "0") int page
    ) {
        String url = String.format("%s/%s/achievements?page=%d",
                ROOT_URL, gameId, page);
        return request.get(url);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<String> game(@PathVariable UUID id) {
        String url = ROOT_URL + "/" + String.valueOf(id);
        return request.get(url);
    }

    @GetMapping(value = "{gameId}/achievements/{id}")
    public ResponseEntity<String> achievement(@PathVariable UUID gameId,
                                              @PathVariable UUID id) {
        return getAchievement(gameId, id);
    }
}
