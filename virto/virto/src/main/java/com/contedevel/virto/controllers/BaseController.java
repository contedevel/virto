package com.contedevel.virto.controllers;

import com.contedevel.virto.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

    @Autowired
    protected RequestService request;
}
