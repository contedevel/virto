package com.contedevel.virto.models;

import java.util.Date;
import java.util.UUID;

public class Experience {
    public UUID id;
    public Date created_on;
    public Date modified_on;
    public UUID user_id;
    public UUID game_id;
    public UUID achievement_id;
    public float value;
}
