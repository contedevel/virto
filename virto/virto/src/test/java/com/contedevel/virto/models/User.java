package com.contedevel.virto.models;

import java.util.Date;
import java.util.UUID;

public class User {
    public UUID id;
    public String first_name;
    public String last_name;
    public String email;
    public Date birthdate;
    public String sex;
    public String role;
}
