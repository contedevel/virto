package com.contedevel.virto.services;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class RequestServiceTestConfig {

    @Bean
    @Primary
    public RequestService getRequestService() {
        return Mockito.mock(RequestService.class);
    }
}