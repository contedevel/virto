package com.contedevel.virto.models;

import java.util.UUID;

public class Achievement {
    public UUID id;
    public String name;
    public String description;
}
