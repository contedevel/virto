package com.contedevel.virto.controllers;

import com.contedevel.virto.models.Achievement;
import com.contedevel.virto.models.Experience;
import com.contedevel.virto.models.User;
import com.contedevel.virto.server.config.ServerConfig;
import com.contedevel.virto.server.models.Page;
import com.contedevel.virto.services.RequestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PlayerControllerTest {

    @Autowired
    private MockMvc mMockMvc;
    @Autowired
    private RequestService mRequestService;
    @Autowired
    private ObjectMapper mMapper;
    private UUID mUserId;
    private UUID mGameId;
    private UUID mAchievementId;

    @Before
    public void setUp() throws Exception {
        mUserId = UUID.randomUUID();
        mGameId = UUID.randomUUID();
        mAchievementId = UUID.randomUUID();

        String achUrl = String.format("%s/games/%s/achievements/%s",
                ServerConfig.GAMES_HOST, mGameId, mAchievementId);
        ResponseEntity<String> achResponse = new ResponseEntity<>(
                getAchievementJson(), HttpStatus.OK);

        String expUrl = String.format("%s/experiences" +
                        "?game_id=%s&achievement_id=%s&asc=false&page=0",
                ServerConfig.EXPERIENCES_HOST, mGameId, mAchievementId);
        ResponseEntity<String> expsResponse = new ResponseEntity<>(
                getExperiencesJson(), HttpStatus.OK);

        String usrUrl = String.format("%s/users/%s", ServerConfig.USERS_HOST, mUserId);
        ResponseEntity<String> usrResponse = new ResponseEntity<>(
                getUserJson(), HttpStatus.OK);

        String expMaxUrl = String.format("%s/experiences/max?user_id=%s&game_id=%s",
                ServerConfig.EXPERIENCES_HOST, mUserId, mGameId);
        ResponseEntity<String> expResponse = new ResponseEntity<>(
                getExperienceJson(), HttpStatus.OK);

        Mockito.when(mRequestService.get(achUrl))
                .thenReturn(achResponse);
        Mockito.when(mRequestService.get(expUrl))
                .thenReturn(expsResponse);
        Mockito.when(mRequestService.get(expMaxUrl))
                .thenReturn(expResponse);
        Mockito.when(mRequestService.get(usrUrl))
                .thenReturn(usrResponse);
    }

    private String getAchievementJson() throws JsonProcessingException {
        Achievement achievement = new Achievement();
        achievement.id = mAchievementId;
        achievement.name = "Name";
        achievement.description = "Description";

        return mMapper.writeValueAsString(achievement);
    }

    private String getUserJson() throws JsonProcessingException {
        User user = new User();
        user.id = mAchievementId;
        user.first_name = "Denis";
        user.last_name = "Sologub";
        user.email = "contedevel2010@gmail.com";
        user.sex = "Male";
        user.role = "Admin";
        user.birthdate = new Date();

        return mMapper.writeValueAsString(user);
    }

    private Experience getExperience() {
        Experience experience = new Experience();
        experience.id = UUID.randomUUID();
        experience.achievement_id = mAchievementId;
        experience.game_id = mGameId;
        experience.user_id = mUserId;
        experience.value = 50f;
        experience.created_on = new Date();
        experience.modified_on = new Date();

        return experience;
    }

    private String getExperienceJson() throws JsonProcessingException {
        return mMapper.writeValueAsString(getExperience());
    }

    private String getExperiencesJson() throws JsonProcessingException {
        List<Experience> experiences = new ArrayList<>(1);
        experiences.add(getExperience());
        Page<Experience> page = new Page<>(experiences, 0, 1);

        return mMapper.writeValueAsString(page);
    }

    @Test()
    public void testOrder() throws Exception {
        testTop();
        testMax();
    }

    private void testTop() throws Exception {
        String url = String.format("/players/top?game_id=%s&achievement_id=%s&count=1",
                mGameId, mAchievementId);
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string(containsString("Denis")));
    }

    private void testMax() throws Exception {
        String url = String.format("/players/%s/games/%s/achievements/max",
                mUserId, mGameId);
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string(containsString(mAchievementId.toString())));
    }
}