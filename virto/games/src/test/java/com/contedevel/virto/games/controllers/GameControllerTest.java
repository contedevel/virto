package com.contedevel.virto.games.controllers;

import com.contedevel.virto.games.models.NewAchievementModel;
import com.contedevel.virto.games.models.NewGameModel;
import com.contedevel.virto.games.services.ValidationService;
import com.contedevel.virto.server.models.GameKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

    @Autowired
    private MockMvc mMockMvc;
    @Autowired
    private ValidationService mValidationService;
    @Autowired
    private ObjectMapper mMapper;
    private NewGameModel mGameModel;
    private NewAchievementModel mAchievementModel;
    private String mGameId;
    private String mAchievementId;

    @Before
    public void setUp() {
        mMapper = new ObjectMapper();
        UUID userId = UUID.randomUUID();
        mGameModel = new NewGameModel("Game", "Test", userId);
        mAchievementModel = new NewAchievementModel("Cool", "Test");
        Mockito.when(mValidationService.validateAdmin(any(UUID.class)))
                .thenReturn(true);
    }

    @Test()
    public void testOrder() throws Exception {
        testPostGame();
        testPostAchievement();
        testGetGames();
        testGetAchievements();
        testGetGame();
        testGetAchievement();
    }

    private void testPostGame() throws Exception {
        String json = mMapper.writeValueAsString(mGameModel);
        final String url = "/games/new";

        MvcResult result = mMockMvc.perform(post(url)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
        String response = result.getResponse()
                .getContentAsString();
        JSONObject obj = new JSONObject(response);
        mGameId = obj.getString(GameKeys.ID);
    }

    private void testPostAchievement() throws Exception {
        String json = mMapper.writeValueAsString(mAchievementModel);
        String url = String.format("/games/%s/achievements/new?user_id=%s",
                mGameId, mGameModel.getUserId());

        MvcResult result = mMockMvc.perform(post(url)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
        String response = result.getResponse()
                .getContentAsString();
        JSONObject obj = new JSONObject(response);
        mAchievementId = obj.getString(GameKeys.ID);
    }

    private void testGetGame() throws Exception {
        mMockMvc.perform(get("/games/" + mGameId))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mGameId)));
    }

    private void testGetGames() throws Exception {
        mMockMvc.perform(get("/games"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mGameId)));
    }

    private void testGetAchievement() throws Exception {
        String url = String.format("/games/%s/achievements/%s", mGameId, mAchievementId);
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mAchievementId)));
    }

    private void testGetAchievements() throws Exception {
        String url = String.format("/games/%s/achievements", mGameId);
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mAchievementId)));
    }
}