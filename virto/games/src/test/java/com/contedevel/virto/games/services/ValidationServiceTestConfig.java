package com.contedevel.virto.games.services;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class ValidationServiceTestConfig {

    @Bean
    @Primary
    public ValidationService getValidationService() {
        return Mockito.mock(ValidationService.class);
    }
}