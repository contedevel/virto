package com.contedevel.virto.games.controllers;

import com.contedevel.virto.games.errors.ServerError;
import com.contedevel.virto.games.errors.GameError;
import com.contedevel.virto.games.models.*;
import com.contedevel.virto.games.models.db.Achievement;
import com.contedevel.virto.games.models.db.Game;
import com.contedevel.virto.games.models.GameManager;
import com.contedevel.virto.games.services.ValidationService;
import com.contedevel.virto.games.utils.GameUtils;
import com.contedevel.virto.server.exceptions.InternalServerException;
import com.contedevel.virto.server.models.Page;
import com.contedevel.virto.server.utils.MathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;
import java.util.UUID;

@EnableWebMvc
@RequestMapping("games")
@RestController("game_controller")
@Validated
public class GameController {

    @Autowired
    private GameManager mGameManager;
    @Autowired
    private ValidationService mValidationService;

    @PostMapping(value = "{gameId}/achievements/new")
    public ResponseEntity<AchievementModel> add(
            @PathVariable UUID gameId,
            @RequestParam("user_id") UUID userId,
            @RequestBody NewAchievementModel newAchievement
    ) {
        if (!mValidationService.validateAdmin(userId)) {
            throw new IllegalArgumentException("Invalid administrator ID");
        }

        Game game = mGameManager.getGameById(gameId);

        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Achievement achievement = newAchievement.toAchievement(gameId);

        if (!mGameManager.add(achievement)) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        AchievementModel model = new AchievementModel(achievement);

        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @PostMapping(value = "new")
    public ResponseEntity<GameModel> add(@RequestBody NewGameModel newGame) {

        if (!mValidationService.validateAdmin(newGame.getUserId())) {
            throw new IllegalArgumentException("Invalid administrator ID");
        }

        Game game = newGame.toGame();

        if (!mGameManager.add(game)) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        return new ResponseEntity<>(new GameModel(game), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<GameModel>> games(@RequestParam(defaultValue = "0") int page) {
        List<Game> games = mGameManager.getGames(page);
        List<GameModel> gameModels = GameUtils.toGameModelList(games);
        long count = mGameManager.getGamesCount();
        long pagesCount = MathUtils.roundUp(count, GameManager.PAGE_SIZE);
        Page<GameModel> jsonPage = new Page<>(gameModels, page, pagesCount);

        return new ResponseEntity<>(jsonPage, HttpStatus.OK);
    }

    @GetMapping(value = "{gameId}/achievements")
    public ResponseEntity<Page<AchievementModel>> achievements(
            @PathVariable UUID gameId,
            @RequestParam(defaultValue = "0") int page
    ) {
        List<Achievement> achievements = mGameManager
                .getAchievementsByGameId(gameId, page);
        List<AchievementModel> achievementModels = GameUtils
                .toAchievementModelList(achievements);
        long count = mGameManager.getAchievementsCount(gameId);
        long pagesCount = MathUtils.roundUp(count, GameManager.PAGE_SIZE);
        Page<AchievementModel> jsonPage =
                new Page<>(achievementModels, page, pagesCount);

        return new ResponseEntity<>(jsonPage, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<GameModel> game(@PathVariable UUID id) {
        Game game = mGameManager.getGameById(id);

        if (game == null) {
            throw new IllegalArgumentException(GameError.INVALID_ID);
        }

        return new ResponseEntity<>(new GameModel(game), HttpStatus.OK);
    }

    @GetMapping(value = "{gameId}/achievements/{id}")
    public ResponseEntity<AchievementModel> achievement(
            @PathVariable UUID gameId,
            @PathVariable UUID id
    ) {
        Game game = mGameManager.getGameById(gameId);

        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Achievement achievement = mGameManager.getAchievementById(id);

        if (achievement == null || !achievement.getGameId().equals(gameId)) {
            throw new IllegalArgumentException(GameError.INVALID_ID);
        }

        return new ResponseEntity<>(new AchievementModel(achievement), HttpStatus.OK);
    }
}
