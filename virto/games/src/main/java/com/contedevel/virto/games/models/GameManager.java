package com.contedevel.virto.games.models;

import com.contedevel.virto.games.models.db.Achievement;
import com.contedevel.virto.games.models.db.Game;
import com.contedevel.virto.games.models.db.GameDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class GameManager implements IGameManager {

    public static final int PAGE_SIZE = 10;

    @Autowired
    private GameDAO mDAO;

    @Override
    public long getGamesCount() {
        return mDAO.getGamesCount();
    }

    @Override
    public long getAchievementsCount(UUID gameId) {
        return mDAO.getAchievementsCount(gameId);
    }

    @Override
    public List<Game> getGames(int pageNumber) {
        return mDAO.getGames(pageNumber, PAGE_SIZE);
    }

    @Override
    public List<Achievement> getAchievementsByGameId(UUID gameId) {
        return mDAO.getAchievements(gameId);
    }

    @Override
    public List<Achievement> getAchievementsByGameId(UUID gameId, int pageNumber) {
        return mDAO.getAchievements(gameId, pageNumber, PAGE_SIZE);
    }

    @Override
    public Game getGameById(UUID id) {
        return mDAO.getGameById(id);
    }

    @Override
    public Achievement getAchievementById(UUID id) {
        return mDAO.getAchievementById(id);
    }

    @Override
    public boolean add(Game game) {
        return mDAO.add(game);
    }

    @Override
    public boolean add(Achievement achievement) {
        return mDAO.add(achievement);
    }
}
