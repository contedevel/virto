package com.contedevel.virto.games.errors;

public final class ServerError {

    public static final String INTERNAL = "Internal server error.";

    private ServerError() {

    }
}
