package com.contedevel.virto.games.models.db;

public abstract class Database {

    public static final String NAME = "virto";
    public static final String SCHEMA = "virto_games";
}
