package com.contedevel.virto.games.utils;

import com.contedevel.virto.games.models.AchievementModel;
import com.contedevel.virto.games.models.GameModel;
import com.contedevel.virto.games.models.db.Achievement;
import com.contedevel.virto.games.models.db.Game;

import java.util.ArrayList;
import java.util.List;

public final class GameUtils {

    private GameUtils() {}

    public static List<GameModel> toGameModelList(List<Game> games) {
        List<GameModel> models = new ArrayList<>(games.size());

        for (Game game : games) {
            models.add(new GameModel(game));
        }

        return models;
    }

    public static List<AchievementModel> toAchievementModelList(
            List<Achievement> achievements
    ) {
        List<AchievementModel> models = new ArrayList<>(achievements.size());

        for (Achievement achievement : achievements) {
            models.add(new AchievementModel(achievement));
        }

        return models;
    }
}
