package com.contedevel.virto.games.models.db;

import java.util.List;
import java.util.UUID;

public interface IGameDAO {
    long getGamesCount();
    long getAchievementsCount(UUID gameId);
    List<Game> getGames(int pageNumber, int pageSize);
    List<Achievement> getAchievements(UUID gameId);
    List<Achievement> getAchievements(UUID gameId, int pageNumber, int pageSize);
    Game getGameById(UUID id);
    Achievement getAchievementById(UUID id);
    boolean add(Game game);
    boolean add(Achievement achievement);
}
