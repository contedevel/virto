package com.contedevel.virto.games.models;

import com.contedevel.virto.games.models.db.Game;
import com.contedevel.virto.server.models.GameKeys;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.UUID;

@JsonRootName("game")
public class GameModel extends NewGameModel {

    @JsonProperty(GameKeys.ID)
    private UUID mId;

    public GameModel(Game game) {
        super(game.getName(), game.getDescription(), game.getUserId());
        mId = game.getId();
    }

    public UUID getId() {
        return mId;
    }
}
