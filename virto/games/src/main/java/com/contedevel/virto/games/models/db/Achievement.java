package com.contedevel.virto.games.models.db;

import com.contedevel.virto.server.models.AchievementKeys;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = AchievementKeys.TABLE_NAME, catalog = Database.NAME,
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {
                    AchievementKeys.NAME,
                    AchievementKeys.GAME_ID})
        }
)
public class Achievement implements Serializable {

    private static final long serialVersionUID = 3769204189080050581L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = AchievementKeys.ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mId;
    @Column(name = AchievementKeys.NAME, nullable = false)
    private String mName;
    @Column(name = AchievementKeys.DESCRIPTION, nullable = false)
    private String mDescription;
    @Column(name = AchievementKeys.GAME_ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mGameId;

    public UUID getId() {
        return mId;
    }

    public Achievement setId(final UUID id) {
        mId = id;
        return this;
    }

    public String getName() {
        return mName;
    }

    public Achievement setName(final String name) {
        mName = name;
        return this;
    }

    public String getDescription() {
        return mDescription;
    }

    public Achievement setDescription(final String description) {
        mDescription = description;
        return this;
    }

    public UUID getGameId() {
        return mGameId;
    }

    public Achievement setGameId(UUID gameId) {
        mGameId = gameId;
        return this;
    }
}
