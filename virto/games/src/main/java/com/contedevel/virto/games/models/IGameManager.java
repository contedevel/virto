package com.contedevel.virto.games.models;

import com.contedevel.virto.games.models.db.Achievement;
import com.contedevel.virto.games.models.db.Game;

import java.util.List;
import java.util.UUID;

public interface IGameManager {
    long getGamesCount();
    long getAchievementsCount(UUID gameId);
    List<Game> getGames(int pageNumber);
    List<Achievement> getAchievementsByGameId(UUID gameId);
    List<Achievement> getAchievementsByGameId(UUID gameId, int pageNumber);
    Game getGameById(UUID id);
    Achievement getAchievementById(UUID id);
    boolean add(Game game);
    boolean add(Achievement achievement);
}
