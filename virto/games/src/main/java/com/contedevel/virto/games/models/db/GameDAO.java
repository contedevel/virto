package com.contedevel.virto.games.models.db;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;

@Repository
public class GameDAO implements IGameDAO {

    @PersistenceContext
    private EntityManager mEntityManager;

    @Override
    public long getGamesCount() {
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = builder.createQuery(Long.class);
        Root<Game> root = cq.from(Game.class);
        cq.select(builder.count(root));
        TypedQuery<Long> tq = mEntityManager.createQuery(cq);

        return tq.getSingleResult();
    }

    @Override
    public long getAchievementsCount(UUID gameId) {
        final String fieldGameId = "mGameId";
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = builder.createQuery(Long.class);
        Root<Achievement> root = cq.from(Achievement.class);
        cq.where(builder.equal(root.get(fieldGameId), gameId));
        cq.select(builder.count(root));
        TypedQuery<Long> tq = mEntityManager.createQuery(cq);

        return tq.getSingleResult();
    }

    @Override
    public List<Game> getGames(int pageNumber, int pageSize) {
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Game> cq = builder.createQuery(Game.class);
        Root<Game> root = cq.from(Game.class);
        cq.select(root);
        TypedQuery<Game> tq = mEntityManager.createQuery(cq);
        tq.setMaxResults(pageSize);
        tq.setFirstResult(pageNumber * pageSize);

        return tq.getResultList();
    }

    @Override
    public List<Achievement> getAchievements(UUID gameId) {
        final String fieldGameId = "mGameId";
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Achievement> cq = builder.createQuery(Achievement.class);
        Root<Achievement> root = cq.from(Achievement.class);
        cq.where(builder.equal(root.get(fieldGameId), gameId));
        cq.select(root);
        TypedQuery<Achievement> tq = mEntityManager.createQuery(cq);

        return tq.getResultList();
    }

    @Override
    public List<Achievement> getAchievements(UUID gameId, int pageNumber, int pageSize) {
        final String fieldGameId = "mGameId";
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Achievement> cq = builder.createQuery(Achievement.class);
        Root<Achievement> root = cq.from(Achievement.class);
        cq.where(builder.equal(root.get(fieldGameId), gameId));
        cq.select(root);
        TypedQuery<Achievement> tq = mEntityManager.createQuery(cq);
        tq.setMaxResults(pageSize);
        tq.setFirstResult(pageNumber * pageSize);

        return tq.getResultList();
    }

    @Override
    public Game getGameById(UUID id) {
        return mEntityManager.find(Game.class, id);
    }

    @Override
    public Achievement getAchievementById(UUID id) {
        return mEntityManager.find(Achievement.class, id);
    }

    @Transactional(noRollbackFor = Exception.class)
    @Override
    public boolean add(Game game) {
        return addEntity(game);
    }

    @Transactional(noRollbackFor = Exception.class)
    @Override
    public boolean add(Achievement achievement) {
        return addEntity(achievement);
    }

    private <T> boolean addEntity(T entity) {

        try {
            mEntityManager.persist(entity);
            mEntityManager.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
