package com.contedevel.virto.games.models;

import com.contedevel.virto.games.models.db.Game;
import com.contedevel.virto.server.constraints.NotNullOrEmpty;
import com.contedevel.virto.server.models.GameKeys;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class NewGameModel {

    @NotNullOrEmpty
    @JsonProperty(GameKeys.NAME)
    private String mName;

    @NotNullOrEmpty
    @JsonProperty(GameKeys.DESCRIPTION)
    private String mDescription;

    @JsonProperty(GameKeys.USER_ID)
    private UUID mUserId;

    public NewGameModel() {

    }

    public NewGameModel(String name, String description, UUID userId) {
        mName = name;
        mDescription = description;
        mUserId = userId;
    }

    @JsonIgnore
    public String getName() {
        return mName;
    }

    @JsonIgnore
    public String getDescription() {
        return mDescription;
    }

    @JsonIgnore
    public UUID getUserId() {
         return mUserId;
    }

    @JsonIgnore
    public Game toGame() {
        return new Game()
                .setName(mName)
                .setDescription(mDescription)
                .setUserId(mUserId);
    }
}
