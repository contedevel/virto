package com.contedevel.virto.games.models;

import com.contedevel.virto.games.models.db.Achievement;
import com.contedevel.virto.server.models.AchievementKeys;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.UUID;

@JsonRootName("achievement")
public class AchievementModel extends NewAchievementModel {

    @JsonProperty(AchievementKeys.ID)
    private UUID mId;

    public AchievementModel(Achievement achievement) {
        super(achievement.getName(), achievement.getDescription());
        mId = achievement.getId();
    }

    public UUID getId() {
        return mId;
    }
}
