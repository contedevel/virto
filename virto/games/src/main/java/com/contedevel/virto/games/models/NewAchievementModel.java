package com.contedevel.virto.games.models;

import com.contedevel.virto.games.models.db.Achievement;
import com.contedevel.virto.server.constraints.NotNullOrEmpty;
import com.contedevel.virto.server.models.AchievementKeys;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class NewAchievementModel {

    @NotNullOrEmpty
    @JsonProperty(AchievementKeys.NAME)
    private String mName;

    @NotNullOrEmpty
    @JsonProperty(AchievementKeys.DESCRIPTION)
    private String mDescription;

    public NewAchievementModel() {

    }

    public NewAchievementModel(String name, String description) {
        mName = name;
        mDescription = description;
    }

    @JsonIgnore
    public String getName() {
        return mName;
    }

    @JsonIgnore
    public String getDescription() {
        return mDescription;
    }

    @JsonIgnore
    public Achievement toAchievement(UUID gameId) {
        return new Achievement()
                .setName(mName)
                .setDescription(mDescription)
                .setGameId(gameId);
    }
}
