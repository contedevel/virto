package com.contedevel.virto.games.errors;

public final class GameError {

    public static final String INVALID_ID = "Invalid ID.";
    public static final String NAME_REQUIRED =
            "The name is required.";
    public static final String DESCRIPTION_REQUIRED =
            "The description is required.";

    private GameError() { }
}
