package com.contedevel.virto.games.models.db;

import com.contedevel.virto.server.models.GameKeys;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = GameKeys.TABLE_NAME, catalog = Database.NAME,
        uniqueConstraints = {
            @UniqueConstraint(columnNames = GameKeys.NAME)
        }
)
public class Game implements Serializable {

    private static final long serialVersionUID = 7346401299988202248L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = GameKeys.ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mId;
    @Column(name = GameKeys.NAME, nullable = false)
    private String mName;
    @Column(name = GameKeys.DESCRIPTION, nullable = false)
    private String mDescription;
    @Column(name = GameKeys.USER_ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mUserId;

    public UUID getId() {
        return mId;
    }

    public Game setId(final UUID id) {
        mId = id;
        return this;
    }

    public String getName() {
        return mName;
    }

    public Game setName(final String name) {
        mName = name;
        return this;
    }

    public String getDescription() {
        return mDescription;
    }

    public Game setDescription(final String description) {
        mDescription = description;
        return this;
    }

    public UUID getUserId() {
        return mUserId;
    }

    public Game setUserId(UUID userId) {
        mUserId = userId;
        return this;
    }
}
