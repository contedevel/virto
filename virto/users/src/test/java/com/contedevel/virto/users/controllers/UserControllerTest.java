package com.contedevel.virto.users.controllers;

import com.contedevel.virto.server.models.UserKeys;
import com.contedevel.virto.users.models.NewUserModel;
import com.contedevel.virto.users.models.db.Role;
import com.contedevel.virto.users.models.db.Sex;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Calendar;


import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mMockMvc;
    private ObjectMapper mMapper;
    private String mUserId;
    private NewUserModel mModel;

    @Before
    public void setUp() {
        mMapper = new ObjectMapper();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 1993);
        calendar.set(Calendar.MONTH, Calendar.AUGUST);
        calendar.set(Calendar.DAY_OF_MONTH, 30);

        mModel = new NewUserModel()
                .setFirstName("Denis")
                .setLastName("Sologub")
                .setEmail("contedevel2010@gmail.com")
                .setBirthdate(calendar.getTime())
                .setSex(Sex.MALE)
                .setRole(Role.ADMIN)
                .setPassword("123@Qwerty")
                .setPasswordConfirmation("123@Qwerty");
    }

    @Test()
    public void testOrder() throws Exception {
        testPost();
        testGet();
        testPut();
        testAuth();
        testProfile();
    }

    private void testPost() throws Exception {
        String json = mMapper.writeValueAsString(mModel);

        MvcResult result = mMockMvc.perform(post("/users/new")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
        String response = result.getResponse()
                .getContentAsString();
        JSONObject obj = new JSONObject(response);
        mUserId = obj.getString(UserKeys.ID);
    }

    private void testPut() throws Exception {
        mModel.setFirstName("Artem");
        String json = mMapper.writeValueAsString(mModel);

        mMockMvc.perform(put("/users/" + mUserId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNoContent());
    }

    private void testGet() throws Exception {
        mMockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Sologub")));
    }

    private void testAuth() throws Exception {
        final String url = "/users/auth?email=contedevel2010@gmail.com"
                + "&password=123@Qwerty";
        mMockMvc.perform(get(url))
                .andExpect(status().isOk());
    }

    private void testProfile() throws Exception {
        mMockMvc.perform(get("/users/" + mUserId))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Sologub")));
    }
}