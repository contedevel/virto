package com.contedevel.virto.users.models;

import com.contedevel.virto.server.constraints.NotNullOrEmpty;
import com.contedevel.virto.server.models.UserKeys;
import com.contedevel.virto.users.errors.UserError;
import com.contedevel.virto.users.models.db.Role;
import com.contedevel.virto.users.models.db.Sex;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Email;

import java.util.Date;

public abstract class BaseUserModel<TChild> {

    @NotNullOrEmpty(message = UserError.INVALID_FIRST_NAME)
    @JsonProperty(UserKeys.FIRST_NAME)
    private String firstName;

    @NotNullOrEmpty(message = UserError.INVALID_LAST_NAME)
    @JsonProperty(UserKeys.LAST_NAME)
    private String lastName;

    @Email
    @JsonProperty(UserKeys.EMAIL)
    private String email;

    @JsonFormat(pattern = "dd.MM.yyyy")
    @JsonProperty(UserKeys.BIRTHDATE)
    private Date birthdate;

    @JsonProperty(UserKeys.SEX)
    private Sex sex;

    @JsonProperty(UserKeys.ROLE)
    private Role role;

    @JsonIgnore
    public String getFirstName() {
        return firstName;
    }

    @JsonIgnore
    public String getLastName() {
        return lastName;
    }

    @JsonIgnore
    public String getEmail() {
        return email;
    }

    @JsonIgnore
    public Date getBirthdate() {
        return birthdate;
    }

    @JsonIgnore
    public Sex getSex() {
        return sex;
    }

    @JsonIgnore
    public Role getRole() {
        return role;
    }

    @SuppressWarnings("unchecked")
    public TChild setFirstName(String firstName) {
        this.firstName = firstName;
        return (TChild)this;
    }

    @SuppressWarnings("unchecked")
    public TChild setLastName(String lastName) {
        this.lastName = lastName;
        return (TChild)this;
    }

    @SuppressWarnings("unchecked")
    public TChild setEmail(String email) {
        this.email = email;
        return (TChild)this;
    }

    @SuppressWarnings("unchecked")
    public TChild setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
        return (TChild)this;
    }

    @SuppressWarnings("unchecked")
    public TChild setSex(Sex sex) {
        this.sex = sex;
        return (TChild)this;
    }

    @SuppressWarnings("unchecked")
    public TChild setRole(Role role) {
        this.role = role;
        return (TChild)this;
    }
}
