package com.contedevel.virto.users.models;

import com.contedevel.virto.server.models.Page;
import com.contedevel.virto.users.models.db.User;

import java.util.UUID;

public interface IUserManager {
    Page<UserModel> getUsers(int pageNumber);
    User getUserById(UUID id);
    boolean hasEmail(String email);
    boolean hasEmail(String email, UUID userId);
    boolean add(User user);
    User set(User user);
    boolean auth(String email, String password);
}
