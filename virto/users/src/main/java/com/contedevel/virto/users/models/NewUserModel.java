package com.contedevel.virto.users.models;

import com.contedevel.virto.server.constraints.FieldsValueMatch;
import com.contedevel.virto.server.models.UserKeys;
import com.contedevel.virto.users.errors.UserError;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@FieldsValueMatch(
    message = UserError.NOT_EQUAL_PASSWORDS,
    field = "password",
    fieldMatch = "passwordConfirmation"
)
public class NewUserModel extends BaseUserModel<NewUserModel> {

    @JsonProperty(UserKeys.PASSWORD)
    private String password;

    @JsonProperty(UserKeys.PASSWORD_CONFIRMATION)
    private String passwordConfirmation;

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public NewUserModel setPassword(String password) {
        this.password = password;
        return this;
    }

    public NewUserModel setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
        return this;
    }
}
