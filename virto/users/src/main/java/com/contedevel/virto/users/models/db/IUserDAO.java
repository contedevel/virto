package com.contedevel.virto.users.models.db;

import java.util.List;
import java.util.UUID;

public interface IUserDAO {
    long getUsersCount();
    List<User> getUsers(int pageNumber, int pageSize);
    User getUserByEmail(String email);
    User getUserById(UUID id);
    boolean add(User user);
    User set(User user);
}
