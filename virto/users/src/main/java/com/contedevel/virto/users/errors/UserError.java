package com.contedevel.virto.users.errors;

public final class UserError {

    public static final String INVALID_ID = "Invalid ID.";
    public static final String NOT_EQUAL_PASSWORDS = "Passwords don't match!";
    public static final String INVALID_PASSWORD =
            "Password must be at least 8 character and contain one character "
            + "from each group [a-z],[A-Z],[0-9],[!@#$&*].";
    public static final String INVALID_FIRST_NAME = "Invalid first name";
    public static final String INVALID_LAST_NAME = "Invalid last name";
    public static final String EMAIL_EXISTS =
            "This email is used already.";

    private UserError() { }
}
