package com.contedevel.virto.users.utils;

import com.contedevel.virto.server.exceptions.IllegalFieldException;
import com.contedevel.virto.users.errors.UserError;
import com.contedevel.virto.users.models.NewUserModel;
import com.contedevel.virto.users.models.UserModel;
import com.contedevel.virto.users.models.db.User;
import com.contedevel.virto.users.models.UserManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class UserUtils {

    private UserUtils() { }

    public static User toUser(UserManager manager, NewUserModel src) {
        checkNewUser(manager, src);

        String password = src.getPassword();
        String salt = PasswordUtils.salt();
        String hash = PasswordUtils.hash(password, salt);
        User dst = new User();
        dst.setFirstName(src.getFirstName());
        dst.setLastName(src.getLastName());
        dst.setEmail(src.getEmail());
        dst.setBirthdate(src.getBirthdate());
        dst.setSex(src.getSex());
        dst.setRole(src.getRole());
        dst.setSalt(salt);
        dst.setHash(hash);

        return dst;
    }

    public static User toUser(UserManager manager, NewUserModel src, User dst) {
        checkExistingUser(manager, src, dst.getId());

        String password = src.getPassword();
        String salt = PasswordUtils.salt();
        String hash = PasswordUtils.hash(password, salt);
        dst.setFirstName(src.getFirstName());
        dst.setLastName(src.getLastName());
        dst.setEmail(src.getEmail());
        dst.setBirthdate(src.getBirthdate());
        dst.setSex(src.getSex());
        dst.setRole(src.getRole());
        dst.setSalt(salt);
        dst.setHash(hash);

        return dst;
    }

    public static List<UserModel> toUserModelList(List<User> users) {
        List<UserModel> models = new ArrayList<>(users.size());

        for (User user : users) {
            models.add(new UserModel(user));
        }

        return models;
    }

    private static void checkNewUser(UserManager manager, NewUserModel user) {
        checkUser(user);

        if (manager.hasEmail(user.getEmail())) {
            throw new IllegalFieldException(UserError.EMAIL_EXISTS);
        }
    }

    public static void checkExistingUser(UserManager manager,
                                          NewUserModel user, UUID userId) {
        checkUser(user);

        if (manager.hasEmail(user.getEmail(), userId)) {
            throw new IllegalFieldException(UserError.EMAIL_EXISTS);
        }
    }

    private static void checkUser(NewUserModel user) {
        String password = user.getPassword();

        if (password == null || !PasswordUtils.isStrongPassword(password)) {
            throw new IllegalFieldException(UserError.INVALID_PASSWORD);
        }
    }
}
