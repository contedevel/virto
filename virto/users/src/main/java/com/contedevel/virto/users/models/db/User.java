package com.contedevel.virto.users.models.db;

import com.contedevel.virto.server.models.UserKeys;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = UserKeys.TABLE_NAME, catalog = Database.NAME,
        uniqueConstraints = {
            @UniqueConstraint(columnNames = UserKeys.EMAIL)
        }
)
public class User implements Serializable {

    private static final long serialVersionUID = -8190974017668449062L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = UserKeys.ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mId;
    @Column(name = UserKeys.FIRST_NAME, nullable = false)
    private String mFirstName;
    @Column(name = UserKeys.LAST_NAME, nullable = false)
    private String mLastName;
    @Column(name = UserKeys.EMAIL, nullable = false)
    private String mEmail;
    @Column(name = UserKeys.BIRTHDATE, nullable = false)
    @Type(type="date")
    private Date mBirthdate;
    @Column(name = UserKeys.SEX, nullable = false)
    @Enumerated(EnumType.STRING)
    private Sex mSex;
    @Column(name = UserKeys.ROLE, nullable = false)
    @Enumerated(EnumType.STRING)
    private Role mRole;
    @Column(name = UserKeys.HASH, nullable = false, length = 32)
    private String mHash;
    @Column(name = UserKeys.SALT, nullable = false, length = 20)
    private String mSalt;

    public UUID getId() {
        return mId;
    }

    public User setId(UUID id) {
        mId = id;
        return this;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public User setFirstName(String firstName) {
        mFirstName = firstName;
        return this;
    }

    public String getLastName() {
        return mLastName;
    }

    public User setLastName(String lastName) {
        mLastName = lastName;
        return this;
    }

    public String getEmail() {
        return mEmail;
    }

    public User setEmail(String email) {
        mEmail = email;
        return this;
    }

    public Date getBirthdate() {
        return mBirthdate;
    }

    public User setBirthdate(Date birthdate) {
        mBirthdate = birthdate;
        return this;
    }

    public Sex getSex() {
        return mSex;
    }

    public User setSex(Sex sex) {
        mSex = sex;
        return this;
    }

    public Role getRole() {
        return mRole;
    }

    public User setRole(Role role) {
        mRole = role;
        return this;
    }

    public String getHash() {
        return mHash;
    }

    public User setHash(String hash) {
        mHash = hash;
        return this;
    }

    public String getSalt() {
        return mSalt;
    }

    public User setSalt(String salt) {
        mSalt = salt;
        return this;
    }
}
