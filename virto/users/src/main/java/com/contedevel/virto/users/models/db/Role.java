package com.contedevel.virto.users.models.db;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Role {
    USER(0, "User"),
    ADMIN(1, "Admin");

    int mId;
    String mRole;

    Role(int id, String role) {
        mId = id;
        mRole = role;
    }

    public int getId() {
        return mId;
    }

    public String getRole() {
        return mRole;
    }

    @JsonValue
    @Override
    public String toString() {
        return mRole;
    }
}
