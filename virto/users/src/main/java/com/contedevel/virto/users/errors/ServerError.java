package com.contedevel.virto.users.errors;

public final class ServerError {

    public static final String INTERNAL = "Internal server error.";
    public static final String INVALID_PAGE = "Invalid page number.";

    private ServerError() {

    }
}
