package com.contedevel.virto.users.models;

import com.contedevel.virto.server.models.UserKeys;
import com.contedevel.virto.users.models.db.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.UUID;

@JsonRootName("user")
public class UserModel extends BaseUserModel<UserModel> {

    @JsonProperty(UserKeys.ID)
    private UUID id;

    public UserModel(User user) {
        id = user.getId();
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setEmail(user.getEmail());
        setBirthdate(user.getBirthdate());
        setSex(user.getSex());
        setRole(user.getRole());
    }
}
