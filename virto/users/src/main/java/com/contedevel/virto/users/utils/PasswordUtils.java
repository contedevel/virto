package com.contedevel.virto.users.utils;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public final class PasswordUtils {

    private static final Random RANDOM = new SecureRandom();
    private static final String CHARSET = "0123456789"
            + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz";

    private PasswordUtils() { }

    public static boolean isStrongPassword(String password) {

        if (password == null) {
            return false;
        }

        final String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        return password.matches(pattern);
    }

    public static String hash(String password, String salt) {

        try {
            byte[] passBytes = hash(password);
            byte[] mixedHash = concat(passBytes, salt.getBytes());
            byte[] hash = hash(mixedHash);
            return DatatypeConverter.printBase64Binary(hash);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String salt() {
        final int length = 16;
        StringBuilder sb = new StringBuilder(length);

        for(int i = 0; i < length; ++i) {
            sb.append(CHARSET.charAt(RANDOM.nextInt(CHARSET.length())));
        }

        return sb.toString();
    }

    private static byte[] hash(byte[] bytes) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        return md5.digest(bytes);
    }

    private static byte[] hash(String str) throws NoSuchAlgorithmException,
            UnsupportedEncodingException {
        byte[] bytes = str.getBytes("UTF-8");
        return hash(bytes);
    }

    private static byte[] concat(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
}
