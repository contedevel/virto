package com.contedevel.virto.users.models;

import com.contedevel.virto.server.models.Page;
import com.contedevel.virto.users.errors.ServerError;
import com.contedevel.virto.users.models.db.User;
import com.contedevel.virto.users.models.db.UserDAO;
import com.contedevel.virto.server.utils.MathUtils;
import com.contedevel.virto.users.utils.PasswordUtils;
import com.contedevel.virto.users.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserManager implements IUserManager {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private UserDAO mDAO;

    @Override
    public Page<UserModel> getUsers(int pageNumber) {

        if (pageNumber < 0) {
            throw new IndexOutOfBoundsException(ServerError.INVALID_PAGE);
        }

        long count = mDAO.getUsersCount();
        long pagesCount = MathUtils.roundUp(count, PAGE_SIZE);

        if (pageNumber != 0 && pageNumber >= pagesCount) {
            throw new IndexOutOfBoundsException(ServerError.INVALID_PAGE);
        }

        List<User> users = mDAO.getUsers(pageNumber, PAGE_SIZE);
        List<UserModel> models = UserUtils.toUserModelList(users);

        return new Page<>(models, pageNumber, pagesCount);
    }

    @Override
    public User getUserById(UUID id) {
        return mDAO.getUserById(id);
    }

    @Override
    public boolean hasEmail(String email) {
        return mDAO.getUserByEmail(email) != null;
    }

    @Override
    public boolean hasEmail(String email, UUID userId) {
        User user = mDAO.getUserByEmail(email);
        return !(user == null || userId.equals(user.getId()));
    }

    @Override
    public boolean add(User user) {
        return mDAO.add(user);
    }

    @Override
    public User set(User user) {
        return mDAO.set(user);
    }

    @Override
    public boolean auth(String email, String password) {
        User user = mDAO.getUserByEmail(email);

        if (user == null) {
            return false;
        }

        String hash = PasswordUtils.hash(password, user.getSalt());

        return hash != null && hash.equals(user.getHash());
    }
}
