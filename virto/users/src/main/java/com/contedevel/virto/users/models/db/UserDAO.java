package com.contedevel.virto.users.models.db;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.UUID;

@Repository
public class
UserDAO implements IUserDAO {

    @PersistenceContext
    private EntityManager mEntityManager;

    @Override
    public long getUsersCount() {
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = builder.createQuery(Long.class);
        Root<User> root = cq.from(User.class);
        cq.select(builder.count(root));
        TypedQuery<Long> tq = mEntityManager.createQuery(cq);

        return tq.getSingleResult();
    }

    @Override
    public List<User> getUsers(int pageNumber, int pageSize) {

        try {
            CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
            CriteriaQuery<User> cq = builder.createQuery(User.class);
            Root<User> root = cq.from(User.class);
            cq.select(root);
            TypedQuery<User> tq = mEntityManager.createQuery(cq);
            tq.setMaxResults(pageSize);
            tq.setFirstResult(pageNumber * pageSize);
            return tq.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getUserByEmail(String email) {
        final String fieldEmail = "mEmail";

        try {
            CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
            CriteriaQuery<User> cq = builder.createQuery(User.class);
            Root<User> root = cq.from(User.class);
            cq.where(builder.equal(root.get(fieldEmail), email));
            cq.select(root);
            TypedQuery<User> tq = mEntityManager.createQuery(cq);
            return tq.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getUserById(UUID id) {
        return mEntityManager.find(User.class, id);
    }

    @Transactional(noRollbackFor = Exception.class)
    @Override
    public boolean add(User user) {

        try {
            mEntityManager.persist(user);
            mEntityManager.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Transactional
    @Override
    public User set(User user) {
        User updatedUser = null;

        try {
            updatedUser = mEntityManager.merge(user);
            mEntityManager.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return updatedUser;
    }
}
