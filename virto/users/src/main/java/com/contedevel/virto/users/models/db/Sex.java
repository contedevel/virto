package com.contedevel.virto.users.models.db;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Sex {
    MALE(0, "Male"),
    FEMALE(1, "Female");

    int mId;
    String mSex;

    Sex(int id, String sex) {
        mId = id;
        mSex = sex;
    }

    public int getId() {
        return mId;
    }

    public String getSex() {
        return mSex;
    }

    @JsonValue
    @Override
    public String toString() {
        return mSex;
    }
}
