package com.contedevel.virto.users.controllers;

import com.contedevel.virto.server.exceptions.InternalServerException;
import com.contedevel.virto.server.models.Page;
import com.contedevel.virto.users.errors.ServerError;
import com.contedevel.virto.users.errors.UserError;
import com.contedevel.virto.users.models.UserModel;
import com.contedevel.virto.users.models.NewUserModel;
import com.contedevel.virto.users.models.db.User;
import com.contedevel.virto.users.models.UserManager;
import com.contedevel.virto.users.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.UUID;

@RequestMapping("users")
@RestController
@EnableWebMvc
@Validated
public class UserController {

    @Autowired
    protected UserManager mUserManager;

    @PostMapping(value = "new")
    public ResponseEntity<UserModel> add(@RequestBody NewUserModel newUser) {
        User user = UserUtils.toUser(mUserManager, newUser);

        if (!mUserManager.add(user)) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        UserModel userModel = new UserModel(user);

        return new ResponseEntity<>(userModel, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<UserModel>> all(@RequestParam(defaultValue = "0") int page) {
        Page<UserModel> modelPage = mUserManager.getUsers(page);

        return new ResponseEntity<>(modelPage, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<UserModel> get(@PathVariable UUID id) {
        User user = mUserManager.getUserById(id);

        if (user == null) {
            throw new IllegalArgumentException(UserError.INVALID_ID);
        }

        UserModel userModel = new UserModel(user);

        return new ResponseEntity<>(userModel, HttpStatus.OK);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<UserModel> put(@PathVariable UUID id,
                                         @RequestBody NewUserModel profile) {
        User user = mUserManager.getUserById(id);

        if (user == null) {
            return add(profile);
        }

        user = UserUtils.toUser(mUserManager, profile, user);
        user = mUserManager.set(user);

        if (user == null) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/auth")
    public ResponseEntity<Boolean> auth(@RequestParam String email,
                                        @RequestParam String password) {
        boolean result = mUserManager.auth(email, password);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
