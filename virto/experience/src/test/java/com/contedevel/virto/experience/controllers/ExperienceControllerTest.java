package com.contedevel.virto.experience.controllers;

import com.contedevel.virto.experience.models.NewExperienceModel;
import com.contedevel.virto.experience.services.ValidationService;
import com.contedevel.virto.server.models.ExperienceKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ExperienceControllerTest {

    @Autowired
    private MockMvc mMockMvc;
    @Autowired
    private ValidationService mValidationService;
    private ObjectMapper mMapper;
    private NewExperienceModel mModel;
    private String mExperienceId;

    @Before
    public void setUp() {
        mMapper = new ObjectMapper();
        UUID userId = UUID.randomUUID();
        UUID gameId = UUID.randomUUID();
        UUID achievementId = UUID.randomUUID();
        float value = 50f;
        mModel = new NewExperienceModel(userId, gameId, achievementId, value);
        Mockito.when(mValidationService.validateUser(any(UUID.class)))
                .thenReturn(true);
        Mockito.when(mValidationService.validateGame(any(UUID.class)))
                .thenReturn(true);
        Mockito.when(mValidationService.validateAchievement(any(UUID.class), any(UUID.class)))
                .thenReturn(true);
    }

    @Test()
    public void testOrder() throws Exception {
        testPost();
        testGet1();
        testGet2();
        testExperience1();
        testExperience2();
        testMax();
        testDelete();
    }

    private void testPost() throws Exception {
        String json = mMapper.writeValueAsString(mModel);
        final String url = "/experiences/new";

        MvcResult result = mMockMvc.perform(post(url)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
        String response = result.getResponse()
                .getContentAsString();
        JSONObject obj = new JSONObject(response);
        mExperienceId = obj.getString(ExperienceKeys.ID);
    }

    private void testGet1() throws Exception {
        String url = String.format("/experiences?user_id=%s&game_id=%s",
                mModel.getUserId(), mModel.getGameId());
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mExperienceId)));
    }

    private void testGet2() throws Exception {
        String url = String.format("/experiences?game_id=%s&achievement_id=%s",
                mModel.getGameId(), mModel.getAchievementId());
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mExperienceId)));
    }

    private void testExperience1() throws Exception {
        String url = "/experiences/" + mExperienceId;
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mExperienceId)));
    }

    private void testExperience2() throws Exception {
        final String format = "/experiences/experience?game_id=%s"
                + "&achievement_id=%s&user_id=%s";
        String url = String.format(format, mModel.getGameId(),
                mModel.getAchievementId(), mModel.getUserId());
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mExperienceId)));
    }


    private void testMax() throws Exception {
        String url = String.format("/experiences/max?user_id=%s&game_id=%s",
                mModel.getUserId(), mModel.getGameId());
        mMockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mExperienceId)));
    }

    private void testDelete() throws Exception {
        String url = "/experiences/" + mExperienceId;
        mMockMvc.perform(delete(url))
                .andExpect(status().isNoContent());
    }
}