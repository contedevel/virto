package com.contedevel.virto.experience.models.db;

import java.util.List;
import java.util.UUID;

public interface IExperienceManager {
    long getCountByAchievementId(UUID gameId, UUID achievementId);
    long getCountByUserId(UUID gameId, UUID userId);
    List<Experience> getAll(int pageNumber, boolean isAsc,
                            UUID gameId, UUID achievementId);
    List<Experience> getAll(int pageNumber, UUID gameId, UUID userId);
    Experience getMax(UUID userId, UUID gameId);
    Experience get(UUID userId, UUID gameId, UUID achievementId);
    Experience getById(UUID id);
    boolean add(Experience experience);
    boolean removeById(UUID id);
}
