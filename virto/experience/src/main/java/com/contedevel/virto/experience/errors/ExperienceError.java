package com.contedevel.virto.experience.errors;

public final class ExperienceError {

    public static final String INVALID_ID = "Invalid ID.";

    private ExperienceError() { }
}
