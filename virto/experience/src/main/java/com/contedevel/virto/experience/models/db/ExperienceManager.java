package com.contedevel.virto.experience.models.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ExperienceManager implements IExperienceManager {

    public static final int PAGE_SIZE = 10;

    @Autowired
    private ExperienceDAO mDAO;

    @Override
    public long getCountByAchievementId(UUID gameId, UUID achievementId) {
        return mDAO.getCountByAchievementId(gameId, achievementId);
    }

    @Override
    public long getCountByUserId(UUID gameId, UUID userId) {
        return mDAO.getCountByUserId(gameId, userId);
    }

    @Override
    public List<Experience> getAll(int pageNumber, boolean isAsc,
                                   UUID gameId, UUID achievementId) {
        return mDAO.getAll(pageNumber, PAGE_SIZE, isAsc, gameId, achievementId);
    }

    @Override
    public List<Experience> getAll(int pageNumber, UUID gameId, UUID userId) {
        return mDAO.getAll(pageNumber, PAGE_SIZE, gameId, userId);
    }

    @Override
    public Experience getMax(UUID userId, UUID gameId) {
        return mDAO.getMax(userId, gameId);
    }

    @Override
    public Experience get(UUID userId, UUID gameId, UUID achievementId) {
        return mDAO.get(userId, gameId, achievementId);
    }

    @Override
    public Experience getById(UUID id) {
        return mDAO.getById(id);
    }

    @Override
    public boolean add(Experience experience) {
        return mDAO.add(experience);
    }

    @Override
    public boolean removeById(UUID id) {
        return mDAO.removeById(id);
    }
}
