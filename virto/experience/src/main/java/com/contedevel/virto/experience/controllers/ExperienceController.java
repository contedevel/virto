package com.contedevel.virto.experience.controllers;

import com.contedevel.virto.experience.errors.ServerError;
import com.contedevel.virto.experience.errors.ExperienceError;
import com.contedevel.virto.experience.services.ValidationService;
import com.contedevel.virto.server.exceptions.InternalServerException;
import com.contedevel.virto.experience.models.*;
import com.contedevel.virto.experience.models.db.Experience;
import com.contedevel.virto.experience.models.db.ExperienceManager;
import com.contedevel.virto.experience.utils.ExperienceUtils;
import com.contedevel.virto.server.models.Page;
import com.contedevel.virto.server.utils.MathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;
import java.util.UUID;

@EnableWebMvc
@RequestMapping("experiences")
@RestController("experience_controller")
public class ExperienceController {

    @Autowired
    private ExperienceManager mExperienceManager;
    @Autowired
    private ValidationService mValidationService;

    @PostMapping(value = "new")
    public ResponseEntity<ExperienceModel> add(
            @RequestBody NewExperienceModel newExperience
    ) {
        UUID userId = newExperience.getUserId();
        UUID gameId = newExperience.getGameId();
        UUID achievementId = newExperience.getAchievementId();

        if (!(mValidationService.validateUser(userId)
                && mValidationService.validateGame(gameId)
                && mValidationService.validateAchievement(gameId, achievementId))) {
            throw new IllegalArgumentException(ExperienceError.INVALID_ID);
        }

        Experience experience = newExperience.toExperience();

        if (!mExperienceManager.add(experience)) {
            throw new InternalServerException(ServerError.INTERNAL);
        }

        return new ResponseEntity<>(new ExperienceModel(experience),
                HttpStatus.CREATED);
    }

    @GetMapping(params = { "game_id", "user_id" })
    public ResponseEntity<Page<ExperienceModel>> experiences(
            @RequestParam("game_id") UUID gameId,
            @RequestParam("user_id") UUID userId,
            @RequestParam(defaultValue = "0") int page
    ) {
        List<Experience> experiences = mExperienceManager
                .getAll(page, gameId, userId);
        long count = mExperienceManager.getCountByUserId(gameId, userId);

        List<ExperienceModel> experienceModels = ExperienceUtils
                .toExperienceModelList(experiences);
        long pagesCount = MathUtils.roundUp(count, ExperienceManager.PAGE_SIZE);
        Page<ExperienceModel> jsonPage = new Page<>(experienceModels,
                page, pagesCount);

        return new ResponseEntity<>(jsonPage, HttpStatus.OK);
    }

    @GetMapping(params = { "game_id", "achievement_id" })
    public ResponseEntity<Page<ExperienceModel>> experiences(
            @RequestParam("game_id") UUID gameId,
            @RequestParam("achievement_id") UUID achievementId,
            @RequestParam(value = "asc", defaultValue = "true") boolean isAsc,
            @RequestParam(defaultValue = "0") int page
    ) {
        List<Experience> experiences = mExperienceManager
                .getAll(page, isAsc, gameId, achievementId);
        long count = mExperienceManager
                .getCountByAchievementId(gameId, achievementId);

        List<ExperienceModel> experienceModels = ExperienceUtils
                .toExperienceModelList(experiences);
        long pagesCount = MathUtils.roundUp(count, ExperienceManager.PAGE_SIZE);
        Page<ExperienceModel> jsonPage = new Page<>(experienceModels,
                page, pagesCount);

        return new ResponseEntity<>(jsonPage, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<ExperienceModel> experience(@PathVariable UUID id) {
        Experience experience = mExperienceManager.getById(id);

        if (experience == null) {
            throw new IllegalArgumentException(ExperienceError.INVALID_ID);
        }

        return new ResponseEntity<>(new ExperienceModel(experience),
                HttpStatus.OK);
    }

    @GetMapping(value = "max", params = { "user_id", "game_id" })
    public ResponseEntity<ExperienceModel> experience(
            @RequestParam("user_id") UUID userId,
            @RequestParam("game_id") UUID gameId
    ) {
        Experience experience = mExperienceManager.getMax(userId, gameId);

        if (experience == null) {
            throw new IllegalArgumentException(ExperienceError.INVALID_ID);
        }

        return new ResponseEntity<>(new ExperienceModel(experience),
                HttpStatus.OK);
    }

    @GetMapping(value = "experience")
    public ResponseEntity<ExperienceModel> experience(
            @RequestParam("user_id") UUID userId,
            @RequestParam("game_id") UUID gameId,
            @RequestParam("achievement_id") UUID achievementId
    ) {
        Experience experience = mExperienceManager
                .get(userId, gameId, achievementId);

        if (experience == null) {
            throw new IllegalArgumentException(ExperienceError.INVALID_ID);
        }

        return new ResponseEntity<>(new ExperienceModel(experience),
                HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable UUID id) {

        if (!mExperienceManager.removeById(id)) {
            throw new IllegalArgumentException(ExperienceError.INVALID_ID);
        }
    }
}
