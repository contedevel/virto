package com.contedevel.virto.experience.errors;

public final class ServerError {

    public static final String INTERNAL = "Internal server error.";

    private ServerError() {

    }
}
