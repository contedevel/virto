package com.contedevel.virto.experience.utils;

import com.contedevel.virto.experience.models.ExperienceModel;
import com.contedevel.virto.experience.models.db.Experience;

import java.util.ArrayList;
import java.util.List;

public final class ExperienceUtils {

    private ExperienceUtils() {}

    public static List<ExperienceModel> toExperienceModelList(
            List<Experience> experiences
    ) {
        List<ExperienceModel> models = new ArrayList<>(experiences.size());

        for (Experience experience : experiences) {
            models.add(new ExperienceModel(experience));
        }

        return models;
    }
}
