package com.contedevel.virto.experience.models;

import com.contedevel.virto.experience.models.db.Experience;
import com.contedevel.virto.server.models.ExperienceKeys;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Date;
import java.util.UUID;

@JsonRootName("experience")
public class ExperienceModel extends NewExperienceModel {

    @JsonProperty(ExperienceKeys.ID)
    private UUID mId;

    @JsonFormat(pattern = "dd.MM.yyyy hh:mm:ss")
    @JsonProperty(ExperienceKeys.CREATED_ON)
    private Date mCreatedOn;

    @JsonFormat(pattern = "dd.MM.yyyy hh:mm:ss")
    @JsonProperty(ExperienceKeys.MODIFIED_ON)
    private Date mModifiedOn;

    public ExperienceModel(Experience experience) {
        super(experience.getUserId(), experience.getGameId(),
                experience.getAchievementId(), experience.getValue());
        mId = experience.getId();
        mCreatedOn = experience.getCreatedOn();
        mModifiedOn = experience.getModifiedOn();
    }

    public UUID getId() {
        return mId;
    }

    public Date getCreatedOn() {
        return mCreatedOn;
    }

    public Date getModifiedOn() {
        return mModifiedOn;
    }
}
