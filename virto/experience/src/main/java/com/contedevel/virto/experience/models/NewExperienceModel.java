package com.contedevel.virto.experience.models;

import com.contedevel.virto.experience.models.db.Experience;
import com.contedevel.virto.server.models.ExperienceKeys;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class NewExperienceModel {

    @JsonProperty(ExperienceKeys.USER_ID)
    private UUID mUserId;

    @JsonProperty(ExperienceKeys.GAME_ID)
    private UUID mGameId;

    @JsonProperty(ExperienceKeys.ACHIEVEMENT_ID)
    private UUID mAchievementId;

    @JsonProperty(ExperienceKeys.VALUE)
    private float mValue;

    public NewExperienceModel() {

    }

    public NewExperienceModel(UUID userId, UUID gameId,
                              UUID achievementId, float value) {
        mUserId = userId;
        mGameId = gameId;
        mAchievementId = achievementId;
        mValue = value;
    }

    public UUID getUserId() {
        return mUserId;
    }

    public UUID getGameId() {
        return mGameId;
    }

    public UUID getAchievementId() {
        return mAchievementId;
    }

    public float getValue() {
        return mValue;
    }

    public Experience toExperience() {
        return new Experience()
                .setUserId(mUserId)
                .setGameId(mGameId)
                .setAchievementId(mAchievementId)
                .setValue(mValue);
    }
}
