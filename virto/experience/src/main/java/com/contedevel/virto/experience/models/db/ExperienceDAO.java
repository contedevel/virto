package com.contedevel.virto.experience.models.db;

import com.contedevel.virto.server.models.db.Restriction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.UUID;

@Repository
public class ExperienceDAO implements IExperienceDAO {

    private static final String F_USER_ID = "mUserId";
    private static final String F_GAME_ID = "mGameId";
    private static final String F_ACHIEVEMENT_ID = "mAchievementId";
    private static final String F_VALUE = "mValue";

    @PersistenceContext
    private EntityManager mEntityManager;

    @Override
    public long getCountByAchievementId(UUID gameId, UUID achievementId) {
        return getCount(
                (CriteriaBuilder builder, Root<Experience> root) ->
                {
                    Predicate equalGame = builder.equal(root.get(F_GAME_ID), gameId);
                    Predicate equalAchievement = builder
                            .equal(root.get(F_ACHIEVEMENT_ID), achievementId);
                    return builder.and(equalGame, equalAchievement);
                });
    }

    @Override
    public long getCountByUserId(UUID gameId, UUID userId) {
        return getCount(
                (CriteriaBuilder builder, Root<Experience> root) ->
                {
                    Predicate equalUser = builder
                            .equal(root.get(F_USER_ID), userId);
                    Predicate equalGame = builder
                            .equal(root.get(F_GAME_ID), gameId);
                    return builder.and(equalUser, equalGame);
                });
    }

    private long getCount(Restriction<Experience> restriction) {
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = builder.createQuery(Long.class);
        Root<Experience> root = cq.from(Experience.class);
        cq.where(restriction.restrict(builder, root));
        cq.select(builder.count(root));
        TypedQuery<Long> tq = mEntityManager.createQuery(cq);

        return tq.getSingleResult();
    }

    @Override
    public List<Experience> getAll(int pageNumber, int pageSize, boolean isAsc,
                                   UUID gameId, UUID achievementId) {
        return getAll(pageNumber, pageSize, isAsc,
                (CriteriaBuilder builder, Root<Experience> root) ->
                {
                    Predicate equalGame = builder
                            .equal(root.get(F_GAME_ID), gameId);
                    Predicate equalAchievement = builder
                            .equal(root.get(F_ACHIEVEMENT_ID), achievementId);
                    return builder.and(equalGame, equalAchievement);
                });
    }

    @Override
    public List<Experience> getAll(int pageNumber, int pageSize,
                                   UUID gameId, UUID userId) {
        return getAll(pageNumber, pageSize, true,
                (CriteriaBuilder builder, Root<Experience> root) ->
                {
                    Predicate equalGame = builder
                            .equal(root.get(F_GAME_ID), gameId);
                    Predicate equalAchievement = builder
                            .equal(root.get(F_USER_ID), userId);
                    return builder.and(equalGame, equalAchievement);
                });
    }

    @Override
    public Experience getMax(UUID userId, UUID gameId) {

        try {
            CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
            CriteriaQuery<Experience> cq = builder.createQuery(Experience.class);
            Root<Experience> root = cq.from(Experience.class);
            Predicate equalUser = builder.equal(root.get(F_USER_ID), userId);
            Predicate equalGame = builder.equal(root.get(F_GAME_ID), gameId);
            cq.where(builder.and(equalUser, equalGame));
            cq.select(root);
            cq.orderBy(builder.desc(root.get("mValue")));
            TypedQuery<Experience> tq = mEntityManager.createQuery(cq);

            return tq.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<Experience> getAll(int pageNumber, int pageSize, boolean isAsc,
                                    Restriction<Experience> restriction) {
        CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
        CriteriaQuery<Experience> cq = builder.createQuery(Experience.class);
        Root<Experience> root = cq.from(Experience.class);
        cq.where(restriction.restrict(builder, root));
        cq.select(root);
        cq.orderBy(isAsc ? builder.asc(root.get(F_VALUE)) :
                builder.desc(root.get(F_VALUE)));
        TypedQuery<Experience> tq = mEntityManager.createQuery(cq);
        tq.setMaxResults(pageSize);
        tq.setFirstResult(pageNumber * pageSize);

        return tq.getResultList();
    }

    @Override
    public Experience get(UUID userId, UUID gameId, UUID achievementId) {

        try {
            CriteriaBuilder builder = mEntityManager.getCriteriaBuilder();
            CriteriaQuery<Experience> cq = builder.createQuery(Experience.class);
            Root<Experience> root = cq.from(Experience.class);
            Predicate equalUser = builder.equal(root.get(F_USER_ID), userId);
            Predicate equalGame = builder.equal(root.get(F_GAME_ID), gameId);
            Predicate equalAchievement = builder
                    .equal(root.get(F_ACHIEVEMENT_ID), achievementId);
            cq.where(builder.and(equalUser, equalGame, equalAchievement));
            cq.select(root);
            TypedQuery<Experience> tq = mEntityManager.createQuery(cq);

            return tq.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Experience getById(UUID id) {
        return mEntityManager.find(Experience.class, id);
    }

    @Transactional(noRollbackFor = Exception.class)
    @Override
    public boolean add(Experience experience) {

        try {
            mEntityManager.persist(experience);
            mEntityManager.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Transactional
    @Override
    public boolean removeById(UUID id) {
        Experience experience = getById(id);

        if (experience != null) {

            try {
                mEntityManager.remove(experience);
                mEntityManager.flush();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
