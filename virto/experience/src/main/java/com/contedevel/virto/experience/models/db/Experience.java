package com.contedevel.virto.experience.models.db;

import com.contedevel.virto.server.models.ExperienceKeys;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = ExperienceKeys.TABLE_NAME, catalog = Database.NAME,
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {
                    ExperienceKeys.GAME_ID,
                    ExperienceKeys.ACHIEVEMENT_ID,
                    ExperienceKeys.USER_ID
            })
        }
)
public class Experience implements Serializable {

    private static final long serialVersionUID = 7346401299988202248L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = ExperienceKeys.ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mId;

    @Column(name = ExperienceKeys.USER_ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mUserId;

    @Column(name = ExperienceKeys.GAME_ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mGameId;

    @Column(name = ExperienceKeys.ACHIEVEMENT_ID, nullable = false)
    @Type(type="pg-uuid")
    private UUID mAchievementId;

    @CreationTimestamp
    @Column(name = ExperienceKeys.CREATED_ON, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date mCreatedOn;

    @UpdateTimestamp
    @Column(name = ExperienceKeys.MODIFIED_ON, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date mModifiedOn;

    @Column(name = ExperienceKeys.VALUE, nullable = false)
    private float mValue;


    public UUID getId() {
        return mId;
    }

    public Experience setId(final UUID id) {
        mId = id;
        return this;
    }

    public UUID getUserId() {
        return mUserId;
    }

    public Experience setUserId(UUID userId) {
        mUserId = userId;
        return this;
    }

    public UUID getGameId() {
        return mGameId;
    }

    public Experience setGameId(UUID gameId) {
        mGameId = gameId;
        return this;
    }

    public UUID getAchievementId() {
        return mAchievementId;
    }

    public Experience setAchievementId(UUID achievementId) {
        mAchievementId = achievementId;
        return this;
    }

    public Date getCreatedOn() {
        return mCreatedOn;
    }

    public Experience setCreatedOn(Date createdOn) {
        mCreatedOn = createdOn;
        return this;
    }

    public Date getModifiedOn() {
        return mModifiedOn;
    }

    public Experience setModifiedOn(Date modifiedOn) {
        mModifiedOn = modifiedOn;
        return this;
    }

    public float getValue() {
        return mValue;
    }

    public Experience setValue(float value) {
        mValue = value;
        return this;
    }
}
